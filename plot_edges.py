import matplotlib.pyplot as plt
import re
import numpy as np

if __name__ == "__main__":

  edge_file = open("edge_record.txt")
  input_edge = edge_file.readlines()
  edge_file.close()

  input_edge = input_edge[1:]

  frame_cnt = 0

  while len(input_edge) > 0:
    frame_cnt = frame_cnt + 1
    print("frame" + str(frame_cnt) + ":")

    #find the next frame idx
    for row_idx in range(1, len(input_edge)):
      if input_edge[row_idx][0:5] == "frame":
        next_frame_row_idx = row_idx
        break

    
    patch_fram_idx = []
    projected_frame_idx = []
    patch_idx = []

    pattern = re.compile(r'\d+')
    #recover the edges information
    for idx in range(1, next_frame_row_idx):
      result1 = pattern.findall(input_edge[idx])
      ii = int(result1[0]) #which frame the patch belongs to
      jj = int(result1[1]) #which frame the patch mapps to
      kk = int(result1[2]) #patch idx

      patch_fram_idx.append(ii)
      projected_frame_idx.append(jj)
      patch_idx.append(kk)

    #used to create the matrix for plotting the edges
    max_i_frame_idx = max(patch_fram_idx)
    max_j_frame_idx = max(projected_frame_idx)

    edge_array = np.zeros([max_i_frame_idx+1, max_j_frame_idx+1])

    #put edges into the plot matrix
    for idx in range(0, len(patch_fram_idx)):
      edge_array[patch_fram_idx[idx]][projected_frame_idx[idx]] = edge_array[patch_fram_idx[idx]][projected_frame_idx[idx]] + 1


    edge_i_uniq_list = []
    edge_j_uniq_list = []
    edge_unqi_val =[]

    for i_idx in range(0, len(edge_array)):
      for j_idx in range(0, len(edge_array[0])):
        if edge_array[i_idx][j_idx] != 0:
          edge_i_uniq_list.append(i_idx)
          edge_j_uniq_list.append(j_idx)
          edge_unqi_val.append(edge_array[i_idx][j_idx])

    fig, ax = plt.subplots()
    ax.scatter(edge_i_uniq_list, edge_j_uniq_list, edge_unqi_val)
    ax.set_xlabel("i_frame idx", fontsize=15)
    ax.set_ylabel("j_frame idx", fontsize=15)
    ax.set_title('edges')
    ax.grid(True)

    fig.savefig("plot_edges/frame"+str(frame_cnt) + ".png")
    plt.close('all')

    input_edge = input_edge[next_frame_row_idx:]

    pass

