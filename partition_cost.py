import matplotlib.pyplot as plt
import re
import numpy as np

EDGE_COMPUTE_COST = 1000

PATCH_TRANSMITTE_COST = 1000

FRAMES_PER_CHIP = 4
PATCH_PER_CHIP = 8
NUM_CHIP = 8

class chip_system:

  chip_coord = []

  frame_chip_map = []
  patch_chip_map = []

  next_chip_to_add_frame = 0 #round robin allocation
  next_chip_to_delet_frame = 0

  next_chip_to_add_patch = 0 #round robin allocation
  next_chip_to_delet_patch = 0

  max_stored_i_frame_idx = -1
  min_stored_i_frame_idx = 0
  max_stored_j_frame_idx = -1
  min_stored_j_frame_idx = 0

  total_transmitte_record = []

  def __init__(self):
    for i in range(0, NUM_CHIP):
      self.frame_chip_map.append([])
      self.patch_chip_map.append([])
      for j in range(0, FRAMES_PER_CHIP):
        self.frame_chip_map[i].append(None)

      for j in range(0, PATCH_PER_CHIP):
        self.patch_chip_map[i].append(None)

      #build chip arragement
      #C  _0_1_2_3
      #R0 |0|1|2|3
      #R1 |7|6|5|4
      
      self.chip_coord = [
        [0 ,0],#0
        [0 ,1],#1
        [0 ,2],#2
        [0 ,3],#3
        [1 ,3],#4
        [1 ,2],#5
        [1 ,1],#6
        [1 ,0] #7
      ]

  def chip_dis(self, chip_i, chip_j):
    distance = self.chip_coord[chip_i][0] - self.chip_coord[chip_j][0] 
    distance = self.chip_coord[chip_i][1] - self.chip_coord[chip_j][1] + distance

    return distance

  def patch_transmite_dis(self, frame_i, frame_j):
    #transmite patch from frame i to frame j
    chip_i_idx = -1
    for chip_idx in range(0, NUM_CHIP):
      for frame_i_idx in range(0, PATCH_PER_CHIP):
        if self.patch_chip_map[chip_idx][frame_i_idx] == frame_i:
          chip_i_idx = chip_idx
          break
      
      if chip_i_idx != -1:
        #chip i found
        break
    
    chip_j_idx = -1
    for chip_idx in range(0, NUM_CHIP):
      for frame_j_idx in range(0, FRAMES_PER_CHIP):
        if self.frame_chip_map[chip_idx][frame_j_idx] == frame_j:
          chip_j_idx = chip_idx
          break
      if chip_j_idx != -1:
        #chip j found
        break

    distance = self.chip_dis(chip_i_idx, chip_j_idx)

    return distance


  def allocation_round_robin(self, max_i_frame_idx, min_i_frame_idx, max_j_frame_idx, min_j_frame_idx):
    #round robin
    
    #record the next chip now, if it is full, try the next chip, if all chips tried, error
    current_next_chip = self.next_chip_to_add_frame

    while max_i_frame_idx > self.max_stored_i_frame_idx:
      #new feature map to be stored

      for idx in range(0, FRAMES_PER_CHIP):
        if self.frame_chip_map[self.next_chip_to_add_frame][idx] == None:
          #found empty spot
          self.frame_chip_map[self.next_chip_to_add_frame][idx] = self.max_stored_i_frame_idx + 1
          self.next_chip_to_add_frame = (self.next_chip_to_add_frame + 1) % NUM_CHIP#switch to next chip after a success insert
          self.max_stored_i_frame_idx = (self.max_stored_i_frame_idx + 1) 
          break
        
        if idx == FRAMES_PER_CHIP - 1:

          if(self.next_chip_to_add_frame == (current_next_chip - 1) % NUM_CHIP):
            #tried all chips and reached end but not yet stored
            raise Exception("chip frame storage full!")
          else:
            self.next_chip_to_add_frame = (self.next_chip_to_add_frame + 1) % NUM_CHIP

    while min_i_frame_idx > self.min_stored_i_frame_idx:
      #need to delet the oldest frame
      for idx in range(0, FRAMES_PER_CHIP):
        if self.frame_chip_map[self.next_chip_to_delet_frame][idx] == self.min_stored_i_frame_idx:
          #found the oldest frame
          self.frame_chip_map[self.next_chip_to_delet_frame][idx] = None
          self.next_chip_to_delet_frame = (self.next_chip_to_delet_frame + 1) % NUM_CHIP
          self.min_stored_i_frame_idx = (self.min_stored_i_frame_idx + 1) 
          break

        if idx == FRAMES_PER_CHIP - 1:
          #reached end but not yet found
          raise Exception("can found the oldest frame " + str (self.next_chip_to_delet_frame) + "to delet!")
        
    
    current_next_chip = self.next_chip_to_add_patch

    while max_j_frame_idx > self.max_stored_j_frame_idx:
      #need to add new patches
      for idx in range(0, PATCH_PER_CHIP):
        if self.patch_chip_map[self.next_chip_to_add_patch][idx] == None:
          #found empty spot
          self.patch_chip_map[self.next_chip_to_add_patch][idx] = self.max_stored_j_frame_idx + 1
          self.next_chip_to_add_patch = (self.next_chip_to_add_patch + 1) % NUM_CHIP #switch to next chip after a success insert
          self.max_stored_j_frame_idx = (self.max_stored_j_frame_idx + 1) 
          break
      
        if idx == PATCH_PER_CHIP - 1:

          if(self.next_chip_to_add_patch == (current_next_chip - 1) % NUM_CHIP):
            #cannot insert patch
            raise Exception("chip " + str (self.next_chip_to_add_frame) + "patch storage full!")
          else:
            self.next_chip_to_add_patch = (self.next_chip_to_add_patch + 1) % NUM_CHIP
        

      
    while min_j_frame_idx > self.min_stored_j_frame_idx:
      #need to delet oldest patches
      for idx in range(0, PATCH_PER_CHIP):
        if self.patch_chip_map[self.next_chip_to_delet_patch][idx] == self.min_stored_j_frame_idx:
          #found
          self.patch_chip_map[self.next_chip_to_delet_patch][idx] = None
          self.next_chip_to_delet_patch = (self.next_chip_to_delet_patch + 1) % NUM_CHIP
          self.min_stored_j_frame_idx = (self.min_stored_j_frame_idx + 1) 
          break
        
        if idx == PATCH_PER_CHIP - 1:
          #reached end but not yet found
          raise Exception("can found the oldest patch " + str (self.next_chip_to_delet_patch) + "to delet!")

  def allocation_sequential(self, max_i_frame_idx, min_i_frame_idx, max_j_frame_idx, min_j_frame_idx):
    #sequential
    
    #record the next chip now, if it is full, try the next chip, if all chips tried, error
    current_next_chip = self.next_chip_to_add_frame

    while max_i_frame_idx > self.max_stored_i_frame_idx:
      #new feature map to be stored

      for idx in range(0, FRAMES_PER_CHIP):
        if self.frame_chip_map[self.next_chip_to_add_frame][idx] == None:
          #found empty spot
          self.frame_chip_map[self.next_chip_to_add_frame][idx] = self.max_stored_i_frame_idx + 1
          self.max_stored_i_frame_idx = (self.max_stored_i_frame_idx + 1) #don't switch chip after insert
          break
        
        if idx == FRAMES_PER_CHIP - 1:

          if(self.next_chip_to_add_frame == (current_next_chip - 1) % NUM_CHIP):
            #tried all chips and reached end but not yet stored
            raise Exception("chip frame storage full!")
          else:
            self.next_chip_to_add_frame = (self.next_chip_to_add_frame + 1) % NUM_CHIP

    current_del_chip = self.next_chip_to_delet_frame
    while min_i_frame_idx > self.min_stored_i_frame_idx:
      #need to delet the oldest frame
      for idx in range(0, FRAMES_PER_CHIP):
        if self.frame_chip_map[self.next_chip_to_delet_frame][idx] == self.min_stored_i_frame_idx:
          #found the oldest frame
          self.frame_chip_map[self.next_chip_to_delet_frame][idx] = None
          self.min_stored_i_frame_idx = (self.min_stored_i_frame_idx + 1) 
          break

        if idx == FRAMES_PER_CHIP - 1:

          if (self.next_chip_to_delet_frame == (current_del_chip - 1) % NUM_CHIP):
            #reached end but not yet found
            raise Exception("can found the oldest frame " + str (self.next_chip_to_delet_frame) + "to delet!")
          else:
            self.next_chip_to_delet_frame = (self.next_chip_to_delet_frame + 1) % NUM_CHIP
        
    
    current_next_chip = self.next_chip_to_add_patch

    while max_j_frame_idx > self.max_stored_j_frame_idx:
      #need to add new patches
      for idx in range(0, PATCH_PER_CHIP):
        if self.patch_chip_map[self.next_chip_to_add_patch][idx] == None:
          #found empty spot
          self.patch_chip_map[self.next_chip_to_add_patch][idx] = self.max_stored_j_frame_idx + 1
          self.max_stored_j_frame_idx = (self.max_stored_j_frame_idx + 1) #don't switch chip after insert
          break
      
        if idx == PATCH_PER_CHIP - 1:

          if(self.next_chip_to_add_patch == (current_next_chip - 1) % NUM_CHIP):
            #cannot insert patch
            raise Exception("chip " + str (self.next_chip_to_add_frame) + "patch storage full!")
          else:
            self.next_chip_to_add_patch = (self.next_chip_to_add_patch + 1) % NUM_CHIP
        

    current_del_chip = self.next_chip_to_delet_patch
    while min_j_frame_idx > self.min_stored_j_frame_idx:
      #need to delet oldest patches
      for idx in range(0, PATCH_PER_CHIP):
        if self.patch_chip_map[self.next_chip_to_delet_patch][idx] == self.min_stored_j_frame_idx:
          #found
          self.patch_chip_map[self.next_chip_to_delet_patch][idx] = None
          self.next_chip_to_delet_patch = (self.next_chip_to_delet_patch + 1) % NUM_CHIP
          self.min_stored_j_frame_idx = (self.min_stored_j_frame_idx + 1) 
          break
        
        if idx == PATCH_PER_CHIP - 1:

          if(self.next_chip_to_delet_patch == (current_del_chip - 1) % NUM_CHIP):
            #reached end but not yet found
            raise Exception("can found the oldest patch " + str (self.next_chip_to_delet_patch) + "to delet!")
          else:
            self.next_chip_to_delet_patch = (self.next_chip_to_delet_patch + 1) % NUM_CHIP


  def cost_eval(self, edge_array, frame_cnt):
    edge_compute_of_chip = []
    patch_trasmitte_of_chip = []

    for chip_idx in range(0, NUM_CHIP):
      edge_compute_cnt = 0
      edge_transmite_cnt = 0

      #count how many frames of patches are projected to the frame stored in this chip as well as the transmitte cost
      for frame_idx in range(0, FRAMES_PER_CHIP):
        if self.frame_chip_map[chip_idx][frame_idx] == None:
          #this frame has been deleted
          continue

        #frame need to be computed
        processed_frame_j_idx = self.frame_chip_map[chip_idx][frame_idx]
        for frame_i_idx in range(self.min_stored_i_frame_idx, self.max_stored_i_frame_idx):
        
          if edge_array[processed_frame_j_idx][frame_i_idx] != 0:
            #non empty edge
            edge_compute_cnt = edge_compute_cnt + edge_array[processed_frame_j_idx][frame_i_idx]
            edge_transmite_cnt = edge_compute_cnt + self.patch_transmite_dis(frame_i_idx, processed_frame_j_idx)


      edge_compute_of_chip.append(edge_compute_cnt)
      patch_trasmitte_of_chip.append(edge_transmite_cnt)

    fig, ax = plt.subplots()
    ax.bar(range(NUM_CHIP), edge_compute_of_chip)
    ax.set_xlabel("chip idx", fontsize=15)
    ax.set_ylabel("compute at that chip", fontsize=15)
    ax.set_title('compute chip distribution')
    ax.grid(True)

    fig.savefig("frame_allocation/frame"+str(frame_cnt) + "compute_distribution.png")


    plt.cla()
    ax.bar(range(NUM_CHIP), patch_trasmitte_of_chip)
    ax.set_xlabel("chip idx", fontsize=15)
    ax.set_ylabel("transmitte to that chip", fontsize=15)
    ax.set_title('transmitte chip distribution')
    ax.grid(True)

    fig.savefig("frame_allocation/frame"+str(frame_cnt) + "transmitte_distribution.png")


    self.total_transmitte_record.append(sum(patch_trasmitte_of_chip))
    plt.cla()
    ax.plot(range(len(self.total_transmitte_record)), self.total_transmitte_record)
    ax.set_xlabel("frame idx")
    ax.set_ylabel("total transmitte at that frmae")
    ax.grid(True)
    
    fig.savefig("frame_allocation/total_transmitte.png")

    plt.close('all')
  


if __name__ == "__main__":

  edge_file = open("edge_record.txt")
  input_edge = edge_file.readlines()
  edge_file.close()

  input_edge = input_edge[1:]

  frame_cnt = 0

  chips = chip_system()

  while len(input_edge) > 0:
    frame_cnt = frame_cnt + 1
    print("frame" + str(frame_cnt) + ":")

    #find the next frame idx
    for row_idx in range(1, len(input_edge)):
      if input_edge[row_idx][0:5] == "frame":
        next_frame_row_idx = row_idx
        break
    
    
    patch_fram_idx = []
    projected_frame_idx = []
    patch_idx = []

    pattern = re.compile(r'\d+')
    #recover the edges information
    for idx in range(1, next_frame_row_idx):
      result1 = pattern.findall(input_edge[idx])
      ii = int(result1[0]) #which frame the patch belongs to
      jj = int(result1[1]) #which frame the patch mapps to
      kk = int(result1[2]) #patch idx

      patch_fram_idx.append(ii)
      projected_frame_idx.append(jj)
      patch_idx.append(kk)

    #used to create the matrix for plotting the edges
    max_i_frame_idx = max(patch_fram_idx)
    min_i_frame_idx = min(patch_fram_idx)
    max_j_frame_idx = max(projected_frame_idx)
    min_j_frame_idx = min(projected_frame_idx)

    edge_array = np.zeros([max_i_frame_idx+1, max_j_frame_idx+1])

    #put edges into the plot matrix
    for idx in range(0, len(patch_fram_idx)):
      edge_array[patch_fram_idx[idx]][projected_frame_idx[idx]] = edge_array[patch_fram_idx[idx]][projected_frame_idx[idx]] + 1


    chips.allocation_sequential(max_i_frame_idx, min_i_frame_idx, max_j_frame_idx, min_j_frame_idx)
    chips.cost_eval(edge_array, frame_cnt)

    input_edge = input_edge[next_frame_row_idx:]

    pass

